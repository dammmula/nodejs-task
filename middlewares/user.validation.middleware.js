const { user } = require('../models/user');
const UserService = require('../services/userService');

const createUserValid = (req, res, next) => {
    if (Object.keys(req.body).length !== Object.keys(user).length - 1) {
        res.status(400).err = 'User entity to create isn\'t valid';
        next();
    }

    for (let key in user) {
        if (key === 'id') {
            if (key in req.body){
                res.status(400).err = 'User entity to create isn\'t valid';
                next();
            }
        } else if (!(key in req.body)) {
            res.status(400).err = 'User entity to create isn\'t valid';
            next();
        }
    }

    if (!UserService.hasValidEmail(req.body.email)) {
        res.status(400).err = 'Email is not valid';
    } else if (!UserService.hasValidPhoneNumber(req.body.phoneNumber)) {
        res.status(400).err = 'Phone number is not valid';
    } if (req.body.password.length < 3) {
        res.status(400).err = 'Password is too short';
    }

    // TODO: Implement validatior for user entity during creation

    next();
}

const updateUserValid = (req, res, next) => {
    for (let key in req.body) {
        if (!(key in user)) {
            res.status(400).err = 'User entity to update isn\'t valid';
            next();
        }
    }
    if (req.body.email &&
        !UserService.hasValidEmail(req.body.email)) {
        res.status(400).err = 'Email is not valid';
    } else if (req.body.phoneNumber &&
        !UserService.hasValidPhoneNumber(req.body.phoneNumber)) {
        res.status(400).err = 'Phone number is not valid';
    } else if (req.body.id) {
        res.status(400).err = 'User entity to update isn\'t valid';
    }

    // TODO: Implement validatior for user entity during update

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;