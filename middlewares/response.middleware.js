const responseMiddleware = (req, res, next) => {

    if (res.statusCode >= 400) {
        res.json({ error: true, message: res.err });
    } else {
        res.status(200).json(res.data);
    }
    next();
   // TODO: Implement middleware that returns result of the query

}

exports.responseMiddleware = responseMiddleware;