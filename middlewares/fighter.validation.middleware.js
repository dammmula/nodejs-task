const FighterService = require('../services/fighterService');
const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    if (!req.body) {
        res.status(400).err = 'Empty request';
        next();
    }
    if (!(Object.keys(req.body).length === Object.keys(fighter).length - 2 ||
        Object.keys(req.body).length === Object.keys(fighter).length - 1)) {
        res.status(400).err = 'User entity to create isn\'t valid';
        next();
    }

    for (let key in fighter) {
        if (key === 'id') {
            if (key in req.body) {
                res.status(400).err = 'Fighter entity to create isn\'t valid';
                next();
            }
        } else if (key === 'health') {
            if (!(key in req.body)) {
                req.body.health = 100;
            }
        } else if (!(key in req.body)) {
            res.status(400).err = 'Fighter entity to create isn\'t valid';
            next();
        }
    }

    if (req.body.power <= 1 || req.body.power >= 100) {
        res.status(400).err = 'Power is not valid';
    } else if (req.body.defense <= 1 || req.body.defense >= 10) {
        res.status(400).err = 'Defense is not valid';
    } else if (req.body.health <= 80 ||
        req.body.health >= 120) {
        res.status(400).err = 'Health is not valid';
    } else if (!FighterService.hasUniqueName(req.body.name)) {
        res.status(400).err = 'This name is already used';
    }
    // TODO: Implement validatior for fighter entity during creation
    next();
}

const updateFighterValid = (req, res, next) => {
    for (let key in req.body) {
        if (!(key in fighter)) {
            res.status(400).err = 'Fighter entity to update isn\'t valid';
            next();
        }
    }
    if (req.body.id) {
        res.status(400).err = 'Fighter entity to update isn\'t valid';
    } if (req.body.power && (req.body.power <= 1 || req.body.power >= 100)) {
        res.status(400).err = 'Power is not valid';
    } else if (req.body.defense && (req.body.defense <= 1 || req.body.defense >= 10)) {
        res.status(400).err = 'Defense is not valid';
    } else if (req.body.health (req.body.health <= 80 ||
        req.body.health >= 120)) {
        res.status(400).err = 'Health is not valid';
    } else if (req.body.name && !FighterService.hasUniqueName(req.body.name)) {
        res.status(400).err = 'This name is already used';
    }
    // TODO: Implement validatior for fighter entity during update
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;