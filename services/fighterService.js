const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    update(id, dataToUpdate) {
        return FighterRepository.update(id, dataToUpdate);
    }

    delete(id) {
        return FighterRepository.delete(id);
    }

    find(id) {
        let fighters = this.getAll();
        let fighter = fighters.find((item) => item.id === id);
        return fighter;
    }

    getAll() {
        return FighterRepository.getAll();
    }
    saveFighter(fighter) {
        if (!fighter) {
            return null;
        }
        FighterRepository.create(fighter);
        return fighter;
    }

    hasUniqueName(name) {
        let fighters = FighterRepository.getAll();
        let found = fighters.find((item) => item.name.toLowerCase() === name.toLowerCase());
        if (found) {
            return false;
        }
        return true;
    }

    // TODO: Implement methods to work with fighters
}

module.exports = new FighterService();