const { UserRepository } = require('../repositories/userRepository');

class UserService {
    update(id, dataToUpdate) {
        return UserRepository.update(id, dataToUpdate);
    }

    delete(id) {
        return UserRepository.delete(id);
    }

    find(id) {
        let users = this.getAll();
        let user = users.find((item) => item.id === id);
        return user;
    }
    getAll() {
        return UserRepository.getAll();
    }

    hasValidEmail(email) {
        if (!this.checkUniqueness('email', email)) {
            return null;
        }
        if (!email.endsWith('@gmail.com')) {
            return null;
        }
        return email;
    }

    hasValidPhoneNumber(phoneNumber) {
        if (!this.checkUniqueness('phoneNumber', phoneNumber)) {
            return null;
        }
        if (!phoneNumber.startsWith('+380')) {
            return null;
        }
        if (phoneNumber.length !== 13) {
            return null;
        }
        return phoneNumber;
    }

    checkUniqueness(key, value) {
        let users = UserRepository.getAll()
        let found = users.find((item) => item[key] === value);
        if (found) {
            return false;
        }
        return true;
    }

    saveUser(user) {
        if(!user) {
            return null;
        }
        const item = UserRepository.create(user);
        return item;
    }

    // TODO: Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();