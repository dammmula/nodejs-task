const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/', createUserValid, (req, res, next) => {
    try {
        if (res.statusCode >= 400) {
            next();
        } else {
            UserService.saveUser(req.body);
        }
        const data = JSON.stringify(req.body);
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/', (req, res, next) => {
    try {
        if (res.statusCode >= 400) {
            next();
        }
        const data = UserService.getAll();
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        if (res.statusCode >= 400) {
            next();
        }
        let data = UserService.find(req.params.id);
        if (!data) {
            res.status(404).err = 'User not found';
        }
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        if (res.statusCode >= 400) {
            next();
        }
        let data = UserService.delete(req.params.id);
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);


router.put('/:id', updateUserValid, (req, res, next) => {
    try {
        if (res.statusCode >= 400) {
            next();
        } else {
            let data = UserService.update(req.params.id, req.body);
            res.data = data;
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);
// TODO: Implement route controllers for user

module.exports = router;