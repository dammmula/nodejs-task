const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        AuthService.login(req.body);
        const data = JSON.stringify(req.body);
        // TODO: Implement login action (get the user if it exist with entered credentials)
        res.data = data;
    } catch (err) {
        res.status(404).err = err.toString();
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;